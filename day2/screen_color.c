#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
	//打开LCD的设备文件
	int fd_lcd;
	fd_lcd = open("/dev/fb0",O_RDWR);
	if(-1 == fd_lcd)
	{
		printf("open LCD failed !\n");
		return -1;
	}
	
	//为LCD进行数据写入
	int color[9] = {0x00FF0000,
					0x00FFDAB9,
					0x00FFFF00,
					0x0000FF00,
					0x0000FFFF,
					0x000000FF,
					0x00FF00FF,
					0x00000000,
					0x00FFFFFF};
	int x,y;
	//屏幕9宫格，屏幕尺寸：480*800
	for(y=0;y<480*1/3;y++)
	{
		for(x=0;x<800*1/3;x++)//X
		{
			write(fd_lcd,&color[0],4);
		}
		
		for(;x<800*2/3;x++)
		{
			write(fd_lcd,&color[1],4);
		}
		
		for(;x<800*3/3;x++)
		{
			write(fd_lcd,&color[2],4);
		}
	}
	
	for(;y<480*2/3;y++)
	{
		for(x=0;x<800*1/3;x++)
		{
			write(fd_lcd,&color[3],4);
		}
		
		for(;x<800*2/3;x++)
		{
			write(fd_lcd,&color[4],4);
		}
		
		for(;x<800*3/3;x++)
		{
			write(fd_lcd,&color[5],4);
		}
	}
	
	for(;y<480*3/3;y++)
	{
		for(x=0;x<800*1/3;x++)
		{
			write(fd_lcd,&color[6],4);
		}
		
		for(;x<800*2/3;x++)
		{
			write(fd_lcd,&color[7],4);
		}
		
		for(;x<800*3/3;x++)
		{
			write(fd_lcd,&color[8],4);
		}
	}
	
	//关闭LCD的设备文件
	close(fd_lcd);
	
	return 0;
}