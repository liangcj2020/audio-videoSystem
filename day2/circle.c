#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
int main()
{
	//打开LCD的设备文件
	int fd_lcd;
	fd_lcd = open("/dev/fb0",O_RDWR);
	if(-1 == fd_lcd)
	{
		printf("open LCD failed !\n");
		return -1;
	}
	
	//为LCD进行数据写入
	int color[9] = {0x00FF0000,
					0x00FFDAB9,
					0x00FFFF00,
					0x0000FF00,
					0x0000FFFF,
					0x000000FF,
					0x00FF00FF,
					0x00000000,
					0x00FFFFFF};
	
    //圆心(400,240)
    int r1 = 80;
    int r2 = 90;
    int r3 = 100;
    int r4 = 120;
    int r5 = 130;
    int r6 = 140;
    int r7 = 150;
    int i,j;
    for(i=0;i<480;i++){
        for(j=0;j<800;j++){
           if( (i-240)*(i-240)+(j-400)*(j-400)<=r1*r1)
            {
                 write(fd_lcd,&color[0],4);
            }else if((i-240)*(i-240)+(j-400)*(j-400)<=r2*r2){
                 write(fd_lcd,&color[1],4);
            }else if((i-240)*(i-240)+(j-400)*(j-400)<=r3*r3){
                 write(fd_lcd,&color[2],4);
            }else if((i-240)*(i-240)+(j-400)*(j-400)<=r4*r4){
                 write(fd_lcd,&color[3],4);
            }else if((i-240)*(i-240)+(j-400)*(j-400)<=r5*r5){
                 write(fd_lcd,&color[4],4);
            }else if((i-240)*(i-240)+(j-400)*(j-400)<=r6*r6){
                 write(fd_lcd,&color[5],4);
            }else if((i-240)*(i-240)+(j-400)*(j-400)<=r7*r7){
                 write(fd_lcd,&color[6],4);
            }else{
                write(fd_lcd,&color[8],4);
            }
            
        }
       
    }


	
	//关闭LCD的设备文件
	close(fd_lcd);
	
	return 0;
}