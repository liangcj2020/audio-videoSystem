#include<stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

 int main(){

	char buffer[80];
	int fd1 = open("one.txt",O_RDWR|O_CREAT,0777);//0777：赋予能操作文件的用户
	int fd2 = open("two.txt",O_RDWR|O_CREAT,0777);

	//首先获取文件大小
	int size = lseek(fd1,0,SEEK_END);
	//回到文件头
	lseek(fd1,0,SEEK_SET);

	int count = size/sizeof(buffer);
	int i;
	for(i=0;i<=count;i++){
		if(i<count){
            read(fd1, buffer, sizeof(buffer));
            write(fd2, buffer, sizeof(buffer));
        }else{
            int len = read(fd1,buffer,sizeof(buffer));//获取最后一次的长度
            printf("len = %d\n", len);
            write(fd2,buffer,len);
        }
	}

	close(fd1);
	close(fd2);
}