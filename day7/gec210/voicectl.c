﻿#include "common.h"

#define REC_CMD "arecord -d3 -c1 -r16000 -traw -fS16_LE cmd.pcm"
#define PCM_FILE "./cmd.pcm"

int id_num, num;
id_num = 0;
pthread_t tid;
int pre_pid, pid;
pre_pid=-1;
void *say_hello(void *args)
{
	if(pid!=pre_pid){
		pthread_cancel(tid); 
		pre_pid = pid;]]\
	}
	if(id_num ==1){//画圆
		system("./quan");
		sleep(1);
		system("./yuan");
	}else if(id_num == 2){//钢琴
		system("killall -9 madplay");
		system("./piano");	
	}else if(id_num == 3){//音乐
		system("killall -9 madplay");
		system("madplay -a +18 1.mp3 &");
	}else if(id_num == 4){//视频		
		system("killall -9 madplay");		
		system("./video .");
	}else if(id_num == 5){//刮刮乐
		system("./ggl");
	}
}

int main(int argc, char const *argv[]) // ./wav2pcm ubuntu-IP
{

	if (argc != 2)
	{
		printf("Usage: %s <ubuntu-IP>\n", argv[0]);
		exit(0);
	}

	int sockfd = init_sock(argv[1]); //由命令行传入一个对方的IP 等效于socket+bind+listen+accept

	printf("waiting for result...\n");
	while (1)
	{
		// 1，调用arecord来录一段音频
		printf("input num to start REC in 3s...\n");
		scanf("%d", &num);
		//启动录音
		system(REC_CMD);
		// 2，将录制好的PCM音频发送给语音识别引擎
		send_pcm(sockfd, PCM_FILE);
		// 3，等待对方回送识别结果（字符串ID）
		xmlChar *id = wait4id(sockfd);
		if (id == NULL)
			continue;

		//将字符串的id转化成为整形的id
		id_num = atoi((char *)id);

		if (id_num > 0)
		{
			int pid = pthread_create(&tid, NULL, say_hello, NULL);
			if(pre_pid==-1)
			{
				pre_pid = pid;
			}
		}

		printf("id: %d\n", id_num);
	}

	close(sockfd);
	return 0;
}
