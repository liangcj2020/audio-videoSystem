#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <linux/input.h>
#include<stdlib.h>
#include <pthread.h>
#include <stdlib.h>


int fd_lcd;
int *map_lcd;
int fd_ts;
int x,y;

int lcd_init()
{
	//打开LCD的设备文件
	fd_lcd = open("/dev/fb0",O_RDWR);
	if(-1 == fd_lcd)
	{
		perror("open LCD failed:");
		return -1;
	}
	
	//映射LCD
	map_lcd = (int *)mmap(	NULL,
							800*480*4,
							PROT_READ|PROT_WRITE,
							MAP_SHARED,
							fd_lcd,
							0);
	if(map_lcd == MAP_FAILED)
	{
		perror("map LCD failed:");
		return -1;
	}
}

int lcd_uinit()
{
	//解除映射
	munmap(map_lcd,800*480*4);
	//关闭LCD的设备文件
	close(fd_lcd);
}

int ts_init(){
    //打开触摸屏的设备文件
	
	fd_ts = open("/dev/input/event0",O_RDWR);
	if(-1 == fd_ts)
	{
		perror("open ts failed :");
		return -1;
	}
    
}

int lcd_draw_bmp(int x_s,int y_s,char *pathname)
{
	//打开图片文件
	int fd_bmp;
	fd_bmp = open(pathname,O_RDWR);
	if(-1 == fd_bmp)
	{
		perror("open bmp failed:");
		return -1;
	}
	
	//读取文件的54字节数据信息
	char buf_head[54];
	
	read(fd_bmp,buf_head,sizeof(buf_head));
	
	//计算图片的宽度和高度
	int bmp_w,bmp_h;
	
	bmp_w = buf_head[18]|buf_head[19]<<8|buf_head[20]<<16|buf_head[21]<<24;
	bmp_h = buf_head[22]|buf_head[23]<<8|buf_head[24]<<16|buf_head[25]<<24;
	// printf("bmp_w=%d,bmp_h=%d\n",bmp_w,bmp_h);
	
	//计算被补充的无效字节的大小
	int n;
	
	if(bmp_w*3%4)
	{
		n = 4-bmp_w*3%4;
	}
	else
	{
		n = 0;
	}
	// printf("n=%d\n",n);
	
	//读取图片文件里的颜色数据
	char buf_bmp[bmp_w*bmp_h*3];
	int i;
	
	for(i=0;i<bmp_h;i++)
	{
		read(fd_bmp,&buf_bmp[bmp_w*3*i],bmp_w*3);
		lseek(fd_bmp,n,SEEK_CUR);
	}
	
	//关闭图片文件
	close(fd_bmp);
	
	//对读取的bmp的颜色数据进行ARGB整合
	int buf_lcd[bmp_w*bmp_h];
	
	for(i=0;i<bmp_w*bmp_h;i++)
	{
		//ARGB			A			R				G				B
		buf_lcd[i] = 0x00<<24|buf_bmp[i*3+2]<<16|buf_bmp[i*3+1]<<8|buf_bmp[i*3];
	}
	
	int x,y;
	
	int buf_lcd_swap[bmp_h][bmp_w];
	
	for(y=0;y<bmp_h;y++)
	{
		for(x=0;x<bmp_w;x++)
		{
			buf_lcd_swap[y][x] = buf_lcd[(bmp_h-1-y)*bmp_w+x];	
		}
	}
	
	//若x_s为-1，则计算x轴的居中显示位置，否则按照实际数值使用
	if(x_s == -1)
	{
		x_s = (800-bmp_w)/2;
	}
	
	//若y_s为-1，则计算y轴的居中显示位置，否则按照实际数值使用
	if(y_s == -1)
	{
		y_s = (480-bmp_h)/2;
	}
	
	for(y=0;y<bmp_h;y++)
	{
		for(x=0;x<bmp_w;x++)
		{
			*(map_lcd+(y+y_s)*800+x+x_s) = buf_lcd_swap[y][x];	
		}
	}
}

int ts_xy(){
    struct input_event ts;
	
	while(1)
	{
		//读取触摸屏的设备文件
		read(fd_ts,&ts,sizeof(ts));		//阻塞函数
		printf("type=%d,code=%d,value=%d\n",ts.type,ts.code,ts.value);
		
		if(ts.type==EV_ABS && ts.code==ABS_X)
		{
			//x = ts.value;			//蓝色
			x = ts.value*800/1024;	//黑色
		}
		
		if(ts.type==EV_ABS && ts.code==ABS_Y)
		{
			//y = ts.value;			//蓝色
			y = ts.value*480/600;	//黑色
		}
		
		if(ts.type==EV_ABS && ts.code==ABS_PRESSURE && ts.value==0)
		{
			break;
		}
		else if(ts.type==EV_KEY && ts.code==BTN_TOUCH && ts.value==0)
		{
			break;
		}
	}
	
	printf("(%d,%d)\n",x,y);

}


void liufu(){
    x=0;
    y=0;
}
void *func()
{
	while(1){
        if(x>0 && y>0){
            int num;
            num = (x/65)+1;
           if(num==1){
			   //可以使用sprintf函数
               lcd_draw_bmp(65*0,0,"key_on.bmp");
               system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d1.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*0,0,"key_off.bmp");
               liufu();
           }else if(num==2){
               lcd_draw_bmp(65*1,0,"key_on.bmp");
               system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d2.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*1,0,"key_off.bmp");
               liufu();
           }else if(num==3){
               lcd_draw_bmp(65*2,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d3.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*2,0,"key_off.bmp");
               liufu();
           }else if(num==4){
               lcd_draw_bmp(65*3,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d4.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*3,0,"key_off.bmp");
               liufu();
           }else if(num==5){
               lcd_draw_bmp(65*4,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d5.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*4,0,"key_off.bmp");
               liufu();
           }else if(num==6){
               lcd_draw_bmp(65*5,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d6.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*5,0,"key_off.bmp");
               liufu();
           }else if(num==7){
               lcd_draw_bmp(65*6,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d7.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*6,0,"key_off.bmp");
               liufu();
           }else if(num==8){
               lcd_draw_bmp(65*7,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d8.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*7,0,"key_off.bmp");
               liufu();
           }else if(num==9){
               lcd_draw_bmp(65*8,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d9.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*8,0,"key_off.bmp");
               liufu();
           }else if(num==10){
               lcd_draw_bmp(65*9,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d10.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*9,0,"key_off.bmp");
               liufu();
           }else if(num==11){
               lcd_draw_bmp(65*10,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d11.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*10,0,"key_off.bmp");
               liufu();
           }else if(num==12){
               lcd_draw_bmp(65*11,0,"key_on.bmp");
                system("killall -9 madplay");		//停掉播放器
               system("madplay -a +18 d12.mp3 &");	//播放音频，后台
               lcd_draw_bmp(65*11,0,"key_off.bmp");
               liufu();
           }
             
            // system("madplay -a +18 d1.mp3 &");	//播放音频，后台
            //system("killall -STOP madplay");	//暂停播放器
            
        }
   
    }
}

void pic_init(){
	int i;
	for(i=0;i<12;i+=1){
		lcd_draw_bmp(65*i,0,"key_off.bmp");
	}
}
int main()
{
	lcd_init();
	pic_init();
    ts_init();

	pthread_t pid;
	pthread_create(&pid,NULL,func,NULL);
	while(1){
        ts_xy();
    }



    close(fd_ts);
	lcd_uinit();
	
	return 0;
}